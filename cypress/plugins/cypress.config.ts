export enum data  {
    left = 'left',
    right = 'right',
    down = 'down',
    up = 'up'
};

export const data1 = [
    data.left,
    data.right,
    data.down,
    data.up
]