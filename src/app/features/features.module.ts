import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturesRoutingModule } from './features-routing.module';
import { CardsTestingModule } from './cards-testing/cards-testing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FeaturesRoutingModule,
    CardsTestingModule
  ]
})
export class FeaturesModule { }
