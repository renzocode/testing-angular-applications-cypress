import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsTestingComponent } from './cards-testing.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  declarations: [
    CardsTestingComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class CardsTestingModule { }
