import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsTestingComponent } from './cards-testing/cards-testing.component';

const routes: Routes = [
  {
    path: 'card-testing',
    component: CardsTestingComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
