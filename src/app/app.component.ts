import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'protractor-example';
  public message = '';
  
  public open(): any {
    setTimeout(() => {
      this.message = 'sending data';
    }, 3000);
  }
}
