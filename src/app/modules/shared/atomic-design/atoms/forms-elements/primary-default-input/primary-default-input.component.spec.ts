import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PrimaryDefaultInputComponent } from './primary-default-input.component';

describe('Primary Default Input Component', () => {
  let component: PrimaryDefaultInputComponent;
  let fixture: ComponentFixture<PrimaryDefaultInputComponent>;
  let rootElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimaryDefaultInputComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryDefaultInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    rootElement = fixture.debugElement;
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
