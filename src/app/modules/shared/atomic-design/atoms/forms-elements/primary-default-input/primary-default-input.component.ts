import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor, 
  NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  CUSTOMERS_TEXTS_TYPES,
  CUSTOMER_TEXT_TYPE,
  FORMS_ELEMENTS_TYPES,
  FORM_ELEMENT_TYPE
} from '../../configurations/forms-elements.config';
import { Subscription } from 'rxjs';
import { CardNumberPipe } from 'src/app/modules/shared/pipes/card-number.pipe';

@Component({
  selector: 'app-primary-default-input',
  templateUrl: './primary-default-input.component.html',
  styleUrls: ['./primary-default-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PrimaryDefaultInputComponent),
      multi: true,
    },
    CardNumberPipe
  ],
})
export class PrimaryDefaultInputComponent implements OnInit, ControlValueAccessor, OnDestroy {

  @ViewChild('zipEl', { static: true }) zipEl: ElementRef;
  @Input() label = 'text input label';
  @Input() isDisabledLabel = false;
  @Input() size = 'm'; // 's', 'm', 'l'
  @Input() marginTop = '';
  @Input() marginBottom = '';
  @Input() customClass = '';
  @Input() width = '';
  @Input() maxLength = 100;
  @Input() placeholderText = '';
  @Input() placeholderIcon = '';
  @Input() shapeType: any = FORM_ELEMENT_TYPE.DEFAULT_FE;
  @Input() customTextType: any = CUSTOMER_TEXT_TYPE.DEFAULT_CTT;
  @Input() isRequired = false;
  @Input() hasErrorMessage = false;
  @Input() errorMessage = '';
  @Input() attributeType = 'text';
  @Input() flow = 'vertical';
  @Input() isDisabledToolTips = true;
  @Input() imagenToolTips = '';

  @Output() textOutput = new EventEmitter();

  private subscriptions: Subscription[] = [];
  public isDisabled = false;
  public value = '';

  constructor(
    element: ElementRef,
    private renderer: Renderer2,
    private cardNumberPipe: CardNumberPipe
  ) { 
    this.zipEl = element;
  }

  onChange = (_: any) => {};
  onTouch = (_: any) => {};

  ngOnInit(): void {
    const isValidType = FORMS_ELEMENTS_TYPES.includes(this.shapeType);
    this.shapeType = isValidType
      ? this.shapeType
      : FORM_ELEMENT_TYPE.DEFAULT_FE;
    this.isDisabledToolTips = this.isDisabledToolTips;

    const isCustomTextValidType = CUSTOMERS_TEXTS_TYPES.includes(this.customTextType);
    this.customTextType = isCustomTextValidType
      ? this.customTextType
      : CUSTOMER_TEXT_TYPE.DEFAULT_CTT;
  }
  
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

  writeValue(value: any): void {
    if (value === '') {
      this.renderer.setProperty(this.zipEl.nativeElement, 'value', value);
    } else {
      this.value = value;
      this.renderer.setProperty(this.zipEl.nativeElement, 'value', value);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  public changeInput(event: Event): void {
    let res = event.target as HTMLInputElement;
    if (this.customTextType === CUSTOMER_TEXT_TYPE.DEFAULT_CTT) {
    } else if (this.customTextType === CUSTOMER_TEXT_TYPE.CUSTOMER_CONFIG_1_CTT) {
      res.value = this.insertCardNumber(res.value);
    } else {
    }
  }

  public insertCardNumber(value: string): any {
    let validCardNumber;
    if (/^[0-9]+$/.test(value.replace(/ /gm, ""))) {
      validCardNumber = value.replace(/\W/gi, "").replace(/(.{4})/g, "$1 ").replace(/\s$/, "");
      if (value.length === this.maxLength) {
        const cardNumber = validCardNumber.replace(/ /gm, "");
        const data = this.cardNumberPipe.transform(cardNumber, 'default');
      } else {

      }
    } else {
      validCardNumber = value.slice(0, value.length - 1);
    }
    return validCardNumber;
  }
}
