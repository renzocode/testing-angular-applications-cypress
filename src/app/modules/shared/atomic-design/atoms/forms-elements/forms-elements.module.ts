import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimaryDefaultInputComponent } from './primary-default-input/primary-default-input.component';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
  declarations: [
    PrimaryDefaultInputComponent
  ],
  imports: [
    CommonModule,
    PipesModule
  ],
  exports: [
    PrimaryDefaultInputComponent
  ]
})
export class FormsElementsModule { }
