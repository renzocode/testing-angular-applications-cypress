import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-primary-default-button',
  templateUrl: './primary-default-button.component.html',
  styleUrls: ['./primary-default-button.component.scss']
})
export class PrimaryDefaultButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
