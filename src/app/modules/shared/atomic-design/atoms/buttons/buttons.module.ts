import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimaryDefaultButtonComponent } from './primary-default-button/primary-default-button.component';


@NgModule({
  declarations: [
    PrimaryDefaultButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PrimaryDefaultButtonComponent
  ]
})
export class ButtonsModule { }
