export enum BUTTON_TYPE {
    DEFAULT_B = 'default-color',
    PRIMARY_CONFIG_1_B = 'conditional-color',
  };
  
  export const B_TYPES = [
    BUTTON_TYPE.DEFAULT_B,
    BUTTON_TYPE.PRIMARY_CONFIG_1_B
  ];
  