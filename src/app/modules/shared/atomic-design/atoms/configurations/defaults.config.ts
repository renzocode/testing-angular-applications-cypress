export enum DEFAULT_POSITION {
  LEFT = 'left',
  RIGHT = 'right',
  CENTER = 'center',
  TOP = 'top',
  BOTTOM = 'bottom'
}

export const DEFAULTS_POSITIONS = [
  DEFAULT_POSITION.TOP,
  DEFAULT_POSITION.CENTER,
  DEFAULT_POSITION.RIGHT,
  DEFAULT_POSITION.LEFT,
  DEFAULT_POSITION.BOTTOM,
];

export const PIXELES_REGEXP = /^[0-9]{1,}px$/;

export interface INgStyle {
  [ key: string ]: any;
}

