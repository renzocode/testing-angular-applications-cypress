export enum FORM_ELEMENT_TYPE {
  DEFAULT_FE = 'default-color',
  PRIMARY_CONFIG_1_I = 'input-color-1',
  PRIMARY_CONFIG_2_I = 'input-color-2',
  PRIMARY_CONFIG_1_S = 'select-color-1',
  PRIMARY_CONFIG_1_CB = 'checkbox-color-1',
  PRIMARY_CONFIG_1_TA = 'textarea-color-1',
}

export const FORMS_ELEMENTS_TYPES = [
  FORM_ELEMENT_TYPE.DEFAULT_FE,
  FORM_ELEMENT_TYPE.PRIMARY_CONFIG_1_I,
  FORM_ELEMENT_TYPE.PRIMARY_CONFIG_1_S,
  FORM_ELEMENT_TYPE.PRIMARY_CONFIG_1_CB,
  FORM_ELEMENT_TYPE.PRIMARY_CONFIG_1_TA
];

export enum CUSTOMER_TEXT_TYPE {
  DEFAULT_CTT = '',
  CUSTOMER_CONFIG_1_CTT = 'card',
  CUSTOMER_CONFIG_2_CTT = 'date',
}

export const CUSTOMERS_TEXTS_TYPES = [
  CUSTOMER_TEXT_TYPE.DEFAULT_CTT,
  CUSTOMER_TEXT_TYPE.CUSTOMER_CONFIG_1_CTT,
  CUSTOMER_TEXT_TYPE.CUSTOMER_CONFIG_2_CTT,
]

export interface ICustomSelectOption {
  value: number;
  text: string;
}

export interface ICustomSelectOption1 {
  value: number;
  text: string;
  text1: string;
}

export interface ICustomNumber {
  value: number;
}
