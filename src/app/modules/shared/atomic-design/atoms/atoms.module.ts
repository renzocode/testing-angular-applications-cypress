import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsElementsModule } from './forms-elements/forms-elements.module';
import { ButtonsModule } from './buttons/buttons.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsElementsModule,
    ButtonsModule
  ],
  exports: [
    FormsElementsModule,
    ButtonsModule
  ]
})
export class AtomsModule { }
