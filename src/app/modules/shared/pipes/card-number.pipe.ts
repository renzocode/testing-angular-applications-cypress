import { Pipe, PipeTransform } from '@angular/core';
import { CardNumber } from '../models/card-number.model';
import { cardNumberErrorMessages } from '../parameters/card-number-error-messages.parameter';

@Pipe({
  name: 'cardNumber'
})
export class CardNumberPipe implements PipeTransform {

  transform(value: string, format?: string, countryCode: string = '', allowEmptyString?: boolean): string {
    let cardNumber: CardNumber;
    let formattedCardNumber = '';

    if (allowEmptyString && value === '') {
      return '';
    }

    if (this.isCardNumberValid(value)) {
      cardNumber = new CardNumber(value);
      formattedCardNumber = cardNumber.getFormattedCardNumber(format, countryCode);
    }
    return formattedCardNumber;
  }

  private isCardNumberValid(cardNumber: any): boolean {
    const VALID_CARD_LENGTH = cardNumber.length;
    let isCardNumberValid = false;
    if (isNaN(cardNumber)) {
      console.error(cardNumberErrorMessages.INVALID_CARD_NUMBER_TYPE_ERR);
    } else if (cardNumber.toString().length < VALID_CARD_LENGTH) {
      console.error(cardNumberErrorMessages.INVALID_CARD_NUMBER_LENGTH_ERR);
    } else {
      isCardNumberValid = true;
    }
    return isCardNumberValid;
  }
}
