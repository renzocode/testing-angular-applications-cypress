import { CardNumberPipe } from './card-number.pipe';

describe('Card Number Pipe', () => {
  
  let cardNumber: CardNumberPipe;

  beforeEach(() => {
    cardNumber = new CardNumberPipe();
  });

  describe('testing for card number of 16 digits', () => {
    it('Should be default formant (xxxx xxxxx xxxx xxxx) card number', () => {
      const testInputCardNumber = '2222111144445555';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'default');
      const expectedResult = '2222 1111 4444 5555';
      expect(transformedCardNumber).toBe(expectedResult);
    }) 

    it('Should be hyphens format (xxxx-xxxxx-xxxx-xxxx) card number', () => {
      const testInputCardNumber = '2222111144445555';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'hyphens');
      const expectedResult = '2222-1111-4444-5555';
      expect(transformedCardNumber).toBe(expectedResult);
    })

    it('Should be dots format (xxxx.xxxxx.xxxx.xxxx) card number', () => {
      const testInputCardNumber = '2222111144445555';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'dots');
      const expectedResult = '2222.1111.4444.5555';
      expect(transformedCardNumber).toBe(expectedResult);
    })
  });

  describe('testing for card number of 19 digits', () => {
    it('Should be default formant (xxxx xxxxx xxxx xxxx xxx) card number', () => {
      const testInputCardNumber = '2222111144445555123';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'default');
      const expectedResult = '2222 1111 4444 5555 123';
      expect(transformedCardNumber).toBe(expectedResult);
    }) 

    it('Should be hyphens format (xxxx-xxxxx-xxxx-xxxx-xxx) card number', () => {
      const testInputCardNumber = '2222111144445555123';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'hyphens');
      const expectedResult = '2222-1111-4444-5555-123';
      expect(transformedCardNumber).toBe(expectedResult);
    })

    it('Should be dots format (xxxx.xxxxx.xxxx.xxxx.xxx) card number', () => {
      const testInputCardNumber = '2222111144445555123';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'dots');
      const expectedResult = '2222.1111.4444.5555.123';
      expect(transformedCardNumber).toBe(expectedResult);
    })
  });

  describe('testing for card number of 20 digits', () => {
    it('Should be default formant (xxxx xxxxx xxxx xxxx xxxx) card number', () => {
      const testInputCardNumber = '22221111444455551237';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'default');
      const expectedResult = '2222 1111 4444 5555 1237';
      expect(transformedCardNumber).toBe(expectedResult);
    }) 

    it('Should be hyphens format (xxxx-xxxxx-xxxx-xxxx-xxxx) card number', () => {
      const testInputCardNumber = '22221111444455551237';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'hyphens');
      const expectedResult = '2222-1111-4444-5555-1237';
      expect(transformedCardNumber).toBe(expectedResult);
    })

    it('Should be dots format (xxxx.xxxxx.xxxx.xxxx.xxxx) card number', () => {
      const testInputCardNumber = '22221111444455551237';
      const transformedCardNumber = cardNumber.transform(testInputCardNumber, 'dots');
      const expectedResult = '2222.1111.4444.5555.1237';
      expect(transformedCardNumber).toBe(expectedResult);
    })
  });
});
