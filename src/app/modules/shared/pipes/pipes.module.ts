import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneNumberPipe } from './phone-number.pipe';
import { CardNumberPipe } from './card-number.pipe';

@NgModule({
  declarations: [
    PhoneNumberPipe,
    CardNumberPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PhoneNumberPipe,
    CardNumberPipe
  ]
})
export class PipesModule { }
