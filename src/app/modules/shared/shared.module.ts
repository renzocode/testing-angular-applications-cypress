import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AtomicDesignModule } from './atomic-design/atomic-design.module';
import { PipesModule } from './pipes/pipes.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AtomicDesignModule,
    PipesModule
  ],
  exports: [
    AtomicDesignModule,
    PipesModule   
  ]
})
export class SharedModule { }


