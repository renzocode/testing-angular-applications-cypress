import { countryDialingCodes } from "../parameters/country-dialing-codes.parameter";
import { phoneNumberErrorMessages } from "../parameters/phone-number-error-messages.parameter";

export class PhoneNumber { 
    private defaultCardNumber: string;
    private firstBlock: string;
    private secondBlock: string;
    private thirdBlock: string;
    private fourthBlock: string;
    private fifthBlock: string;

    constructor(cardNumber: string) {
      this.defaultCardNumber = cardNumber;
      this.firstBlock = this.getFirstNumber(cardNumber);
      this.secondBlock = this.getSecondNumber(cardNumber);
      this.thirdBlock = this.getThirdNumber(cardNumber);
      this.fourthBlock = this.getFourthNumber(cardNumber);
      this.fifthBlock = this.getFifthNumber(cardNumber);
    }

    private getFirstNumber(phoneNumber: string): string {
      return phoneNumber.substring(0, 4);
    }

    private getSecondNumber(phoneNumber: string): string {
      return phoneNumber.substring(4, 8);
    }

    private getThirdNumber(phoneNumber: string): string {
      return phoneNumber.substring(8, 12);
    }

    private getFourthNumber(phoneNumber: string): string {
      return phoneNumber.substring(12, 16);
    }
    
    private getFifthNumber(phoneNumber: string): string {
      return phoneNumber.substring(16);
    }

    private getDefaultFormattedCardNumber(): string {
      let formatCard;
      formatCard = `${ this.firstBlock } ${ this.secondBlock } ${ this.thirdBlock } ${ this.fourthBlock }`;
      if (this.defaultCardNumber.length > 16) {
        formatCard = formatCard + ` ${ this.fifthBlock }`;
      }
      return formatCard;
    }

    private getHyphensFormattedPhoneNumber(): string {
      let formatCard;
      formatCard = `${ this.firstBlock }-${ this.secondBlock }-${ this.thirdBlock }-${ this.fourthBlock }`;
      if (this.defaultCardNumber.length > 16) {
        formatCard = formatCard + `-${ this.fifthBlock }`;
      }
      return formatCard;
    }

    private getDotsFormattedPhoneNumber(): string {
      let formatCard;
      formatCard = `${ this.firstBlock }.${ this.secondBlock }.${ this.thirdBlock }.${ this.fourthBlock }`;
      if (this.defaultCardNumber.length > 16) {
        formatCard = formatCard + `.${ this.fifthBlock }`;
      }
      return formatCard;
    }

    private getInternationCountryCodeStr(countryCode: string): string {
      countryCode = countryCode.toUpperCase();
      let countryDialingCode = '';

    //   if (countryDialingCodes[countryCode]) {
    //     countryDialingCode = `+${ countryDialingCodes[countryCode] }`;
    //   } else {
    //     console.warn(phoneNumberErrorMessages.INVALID_COUNTRY_CODE_WARN);
    //   }
      return countryDialingCode;
    }

    private getFormattedPhoneNumberStr(format = 'default', countryCode?: string): string {
      let formattedPhoneNumber = '';

      switch (format.toLocaleLowerCase()) {
        case 'default':
          formattedPhoneNumber = this.getDefaultFormattedCardNumber();
          break;
        case 'dots':
          formattedPhoneNumber = this.getDotsFormattedPhoneNumber();
          break;
        case 'hyphens':
          formattedPhoneNumber = this.getHyphensFormattedPhoneNumber();
          break;
        default:
          console.warn(phoneNumberErrorMessages.INVALID_COUNTRY_CODE_WARN);
          formattedPhoneNumber = this.getHyphensFormattedPhoneNumber();
      }

      return formattedPhoneNumber;
    }

    public getFormattedPhoneNumber(format = 'default', countryCode?: string): string {
      let formattedPhoneNumber: string = this.getFormattedPhoneNumberStr(format);
      console.log(formattedPhoneNumber);
      return formattedPhoneNumber;
    }
}