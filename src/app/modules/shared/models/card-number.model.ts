import { cardNumberErrorMessages } from "../parameters/card-number-error-messages.parameter";
import { countryDialingCodes } from "../parameters/country-dialing-codes.parameter";

export class CardNumber { 
    private defaultCardNumber: string;
    private firstBlock: string;
    private secondBlock: string;
    private thirdBlock: string;
    private fourthBlock: string;
    private fifthBlock: string;

    constructor(cardNumber: string) {
      this.defaultCardNumber = cardNumber;
      this.firstBlock = this.getFirstNumber(cardNumber);
      this.secondBlock = this.getSecondNumber(cardNumber);
      this.thirdBlock = this.getThirdNumber(cardNumber);
      this.fourthBlock = this.getFourthNumber(cardNumber);
      this.fifthBlock = this.getFifthNumber(cardNumber);
    }

    private getFirstNumber(cardNumber: string): string {
      return cardNumber.substring(0, 4);
    }

    private getSecondNumber(cardNumber: string): string {
      return cardNumber.substring(4, 8);
    }

    private getThirdNumber(cardNumber: string): string {
      return cardNumber.substring(8, 12);
    }

    private getFourthNumber(cardNumber: string): string {
      return cardNumber.substring(12, 16);
    }
    
    private getFifthNumber(cardNumber: string): string {
      return cardNumber.substring(16);
    }

    private getDefaultFormattedCardNumber(): string {
      let formatCard;
      formatCard = `${ this.firstBlock } ${ this.secondBlock } ${ this.thirdBlock } ${ this.fourthBlock }`;
      if (this.defaultCardNumber.length > 16) {
        formatCard = formatCard + ` ${ this.fifthBlock }`;
      }
      return formatCard;
    }

    private getHyphensFormattedCardNumber(): string {
      let formatCard;
      formatCard = `${ this.firstBlock }-${ this.secondBlock }-${ this.thirdBlock }-${ this.fourthBlock }`;
      if (this.defaultCardNumber.length > 16) {
        formatCard = formatCard + `-${ this.fifthBlock }`;
      }
      return formatCard;
    }

    private getDotsFormattedCardNumber(): string {
      let formatCard;
      formatCard = `${ this.firstBlock }.${ this.secondBlock }.${ this.thirdBlock }.${ this.fourthBlock }`;
      if (this.defaultCardNumber.length > 16) {
        formatCard = formatCard + `.${ this.fifthBlock }`;
      }
      return formatCard;
    }

    private getInternationCountryCodeStr(countryCode: string): string {
      countryCode = countryCode.toUpperCase();
      let countryDialingCode = '';

    //   if (countryDialingCodes[countryCode]) {
    //     countryDialingCode = `+${ countryDialingCodes[countryCode] }`;
    //   } else {
    //     console.warn(cardNumberErrorMessages.INVALID_COUNTRY_CODE_WARN);
    //   }
      return countryDialingCode;
    }

    private getFormattedCardNumberStr(format = 'default', countryCode?: string): string {
      let formattedCardNumber = '';

      switch (format.toLocaleLowerCase()) {
        case 'default':
          formattedCardNumber = this.getDefaultFormattedCardNumber();
          break;
        case 'dots':
          formattedCardNumber = this.getDotsFormattedCardNumber();
          break;
        case 'hyphens':
          formattedCardNumber = this.getHyphensFormattedCardNumber();
          break;
        default:
          console.warn(cardNumberErrorMessages.INVALID_COUNTRY_CODE_WARN);
          formattedCardNumber = this.getHyphensFormattedCardNumber();
      }

      return formattedCardNumber;
    }

    public getFormattedCardNumber(format = 'default', countryCode?: string): string {
      let formattedCardNumber: string = this.getFormattedCardNumberStr(format);
      console.log(formattedCardNumber);
      return formattedCardNumber;
    }
}