export enum issuingCountry {
  DEFAULT = '',
  NL = 'NL',
  BE = 'BE',
  FR = 'FR',
  CN = 'CN',
  US = 'US',
  GB = 'GB',
  BR = 'BR',
  CA = 'CA',
  PL = 'PL',
  ES = 'ES',
  AU = 'AU',
  IN = 'IN',
  IL = 'IL',
  MX = 'MX',
  AZ = 'AZ',
  TW = 'TW',
  MU = 'MU',
  RU = 'RU',
  GT = 'GT',
  DK = 'DK',
  JP = 'JP'
}
