export const cardNumberErrorMessages = {
  INVALID_CARD_NUMBER_TYPE_ERR: `The card number type you have entered is
    invalid. The should number in a string or number format.`,
  INVALID_CARD_NUMBER_LENGTH_ERR: `The card number you have entered is not
    the proper length. It should be 10 characters long.`,
  INVALID_COUNTRY_CODE_WARN: `Unrecognized country code entered.`,
  INVALID_FORMAT_WARN: `Unrecognized card format entered. Using default
    XXXX XXX XXXX XXXX format.`
};