export enum securityCode  {
  CVE = 'cve',
  CVV2 = 'cvv2',
  CVC3 = 'cvc3',
  CID = 'cid',
  CVC2 = 'cvc2',
  CVC = 'cvc',
  CVD = 'cvd'
}

export const securityCodeList = [
  securityCode.CVE,
  securityCode.CVV2,
  securityCode.CVC3,
  securityCode.CID,
  securityCode.CVC2,
  securityCode.CVC,
  securityCode.CVD
];